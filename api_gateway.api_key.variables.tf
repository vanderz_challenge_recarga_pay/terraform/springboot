variable "api_gateway_api_key_name" {
  default = "RecargaPay"
}

variable "api_gateway_api_key_description" {
  default = null
}

variable "api_gateway_api_key_enabled" {
  default = true
}
