# A nomenclatura do output deve seguir o padrao api_gateway_<nome_do_recurso>_path
output "api_gateway_clientes_path" {

    # Modificar conforme nome do módulo do recurso
    value = module.clientes.resource_path
}