locals {
    api_gateway_deployment_triggers = {
        "redeployment" = var.api_gateway_files_hash
    }
    api_gateway_stage_name = var.api_gateway_stage_name
    api_gateway_deployment_stage_name = null
    api_gateway_deployment_variables = {}
    api_gateway_stage_variables = var.api_gateway_stage_variables
}
