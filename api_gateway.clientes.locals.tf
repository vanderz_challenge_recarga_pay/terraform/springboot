locals {
  clientes_path = "/clientes"
  clientes_path_part = element(reverse(split("/",local.clientes_path)),0)

  clientes_methods = {
    "GET" = {
      authorization_scopes = []
      request_models = {}
      request_parameters = {}
      authorization = "NONE"
  	  request_validator_id = null
	    api_key_required = true 
      method_responses = {
        200 =  {
          response_models = {"application/json" = "Empty"},
          response_parameters = {}
        }
      }
    }
  }

  clientes_integrations = {
    "GET" = {
      cache_key_parameters = []
	    connection_id = module.vpc_link.vpc_link_id
	    connection_type = "VPC_LINK"
      integration_http_method = "GET"
      type = "HTTP_PROXY"
      content_handling = null
      uri = "http://${module.fargate.aws_lb_dns_name}:${var.fargate_app_port}/${local.clientes_path_part}"
      request_parameters = {}
      passthrough_behavior = "WHEN_NO_MATCH"
      request_templates = {}
      integration_responses = {
        200 = {
          response_templates = {},
          response_parameters = {},
          selection_pattern = ""
        }       
      }
    }
  }
}