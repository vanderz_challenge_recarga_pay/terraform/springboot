variable "api_gateway_vpc_link_name" {
  default = "recarga_pay"
}

variable "api_gateway_vpc_link_description" {
  default = "Connect to NLB"
}
