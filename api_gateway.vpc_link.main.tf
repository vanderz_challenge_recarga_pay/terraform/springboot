module "vpc_link" {
    source = "git::https://gitlab.com/vanderz_challenge_recarga_pay/terraform/modules/aws/api_gateway/vpc_link?ref=v1.0.0"

    depends_on = [ module.fargate ]

    vpc_link_name = var.api_gateway_vpc_link_name
    vpc_link_description = var.api_gateway_vpc_link_description
    vpc_link_target_arns = local.api_gateway_vpc_link_target_arns
}