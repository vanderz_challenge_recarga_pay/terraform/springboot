variable "api_gateway_rest_api_tags" {
  default = {}
}

variable "api_gateway_stage_name" {
  default  = "prd"
}

variable "api_gateway_stage_cache_cluster_enabled" {
  default = false
}

variable "api_gateway_stage_cache_cluster_size" {
  default = 0.5
}

variable "api_gateway_stage_variables" {
  default = {}
}

variable "api_gateway_files_hash" {}
