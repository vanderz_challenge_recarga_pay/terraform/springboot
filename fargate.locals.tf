locals {
    up_policy_name = "${var.fargate_app_name}_scale_up"
    down_policy_name = "${var.fargate_app_name}_scale_down"
    service_cpu_high_name = "${var.fargate_app_name}_cpu_utilization_high"
    service_cpu_low_name = "${var.fargate_app_name}_cpu_utilization_low"
    ecs_cluster_name = "${var.fargate_app_name}-cluster"
    task_definition_family_name = "${var.fargate_app_name}-task"
    ecs_service_name = "${var.fargate_app_name}-service"
    ecs_task_execution_role_name = "${var.fargate_app_name}_EcsTaskExecutionRole"
    ecs_task_execution_role_policy_arn = {
      "ECS" = {
        policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
      }
    }
    ecs_tasks_security_group_name = "${var.fargate_app_name}-ecs-security-group"
    lb_name = "${var.fargate_app_name}-load-balancer"
    lb_target_group_name = "${var.fargate_app_name}-target-group"
    lb_security_group_name = "${var.fargate_app_name}-lb-security-group"
    lb_tags = {
      Name = "${var.fargate_app_name}-lb"
    }
    log_group_name = "/ecs/${var.fargate_app_name}"
    log_group_tags = {
      Name = "${var.fargate_app_name}-log-group"
    }
    log_stream = "${var.fargate_app_name}-log-stream"
    app_image = "${var.account}.dkr.ecr.${var.region}.amazonaws.com/${var.fargate_app_image}:${var.fargate_app_version}"
    container_name = "${var.fargate_app_name}-app"
    container_vars = {
      container_name = local.container_name
      app_image      = local.app_image
      app_port       = var.fargate_app_port
      fargate_cpu    = var.fargate_cpu
      fargate_memory = var.fargate_memory
      aws_region     = var.region
      log_group_name = local.log_group_name
      aws_account = var.account
    }
}