# Nome do módulo deve ser o <nome_do_recurso>
module "clientes" {
    source = "git::https://gitlab.com/vanderz_challenge_recarga_pay/terraform/modules/aws/api_gateway/resource?ref=v1.0.0"

    api_id = module.api.rest_api_id
    parent_id = module.api.root_resource_id

    # Modificar conforme nome definido no arquivo api_gateway.*.locals.tf
    path_part = local.clientes_path_part
}

# Nome do módulo deve ser o <nome_do_recurso>_http_methods
module "clientes_method" {
    source = "git::https://gitlab.com/vanderz_challenge_recarga_pay/terraform/modules/aws/api_gateway/method?ref=v1.0.0"

    depends_on = [module.vpc_link]

    api_id = module.api.rest_api_id

    # Modificar conforme nome do módulo do recurso
    resource_id = module.clientes.resource_id

    # Modificar conforme nome definido no arquivo api_gateway.*.locals.tf
    for_each = local.clientes_methods

    http_method = each.key
    authorization_scopes = each.value.authorization_scopes
    authorization = each.value.authorization
    request_validator_id = each.value.request_validator_id
    api_key_required = each.value.api_key_required
    request_models = each.value.request_models
    request_parameters = each.value.request_parameters
    method_responses = each.value.method_responses
}

# Nome do módulo deve ser o <nome_do_recurso>_http_methods
module "clientes_integration" {
    source = "git::https://gitlab.com/vanderz_challenge_recarga_pay/terraform/modules/aws/api_gateway/integration?ref=v1.0.0"

    depends_on = [module.vpc_link]

    api_id = module.api.rest_api_id

    # Modificar conforme nome do módulo do recurso
    resource_id = module.clientes.resource_id

    # Modificar conforme nome definido no arquivo api_gateway.*.locals.tf
    for_each = local.clientes_integrations

    http_method = each.key
    cache_key_parameters = each.value.cache_key_parameters
    connection_id = each.value.connection_id
    connection_type = each.value.connection_type
    integration_http_method = each.value.integration_http_method
    type = each.value.type
    content_handling = each.value.content_handling
    uri = each.value.uri
    request_parameters = each.value.request_parameters
    passthrough_behavior = each.value.passthrough_behavior
    request_templates = each.value.request_templates
    integration_responses = each.value.integration_responses
}
