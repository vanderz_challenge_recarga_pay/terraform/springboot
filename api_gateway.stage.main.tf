module "stage" {
    source = "git::https://gitlab.com/vanderz_challenge_recarga_pay/terraform/modules/aws/api_gateway/stage?ref=v1.0.0"

    depends_on = [  
                    module.clientes_integration
                 ]

    api_id = module.api.rest_api_id
    deployment_stage_name = local.api_gateway_deployment_stage_name
    deployment_triggers = local.api_gateway_deployment_triggers
    deployment_variables = local.api_gateway_deployment_variables
    stage_name = local.api_gateway_stage_name
    stage_variables = local.api_gateway_stage_variables
    stage_tags = var.api_gateway_rest_api_tags
    cache_cluster_enabled = var.api_gateway_stage_cache_cluster_enabled
    cache_cluster_size = var.api_gateway_stage_cache_cluster_size
}