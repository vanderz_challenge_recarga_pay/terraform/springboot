module "fargate" {
  source = "git::https://gitlab.com/vanderz_challenge_recarga_pay/terraform/modules/aws/fargate?ref=v1.1.0"

  # network
  vpc_map = var.fargate_vpc_map
  subnets_map = var.fargate_subnets_map
  az_count = var.fargate_az_count

  # ECS
  ecs_cluster_name = local.ecs_cluster_name
  ecs_service_name = local.ecs_service_name
  container_name = local.container_name
  container_vars = local.container_vars
  app_name = var.fargate_app_name
  app_version = var.fargate_app_version
  app_image = local.app_image
  app_port = var.fargate_app_port
  app_count = var.fargate_app_count
  health_check_path = var.fargate_health_check_path
  health_check_grace_period_seconds = var.fargate_health_check_grace_period_seconds
  healthy_threshold = var.fargate_healthy_threshold
  unhealthy_threshold = var.fargate_unhealthy_threshold
  health_interval = var.fargate_health_interval
  health_matcher = var.fargate_health_matcher
  health_timeout = var.fargate_health_timeout
  cpu = var.fargate_cpu
  memory = var.fargate_memory
  task_definition_family_name = local.task_definition_family_name
  ecs_sg_ingress_cidr_blocks = var.fargate_ecs_sg_ingress_cidr_blocks
  ecs_task_execution_role_name = local.ecs_task_execution_role_name
  ecs_task_execution_role_policy_arn = local.ecs_task_execution_role_policy_arn
  ecs_tasks_security_group_name = local.ecs_tasks_security_group_name
  ecs_tasks_security_group_description = var.fargate_ecs_tasks_security_group_description
  
  # Load Balancer
  lb_name = local.lb_name
  lb_type = var.fargate_lb_type
  lb_enable_deletion_protection = var.fargate_lb_enable_deletion_protection
  lb_internal = var.fargate_lb_internal
  lb_target_group_name = local.lb_target_group_name
  lb_port = var.fargate_lb_port
  lb_protocol = var.fargate_lb_protocol
  lb_listeners = var.fargate_lb_listeners
  lb_tags = local.lb_tags

  # Logging
  log_retention_in_days = var.fargate_log_retention_in_days
  log_group_name = local.log_group_name
  log_group_tags = local.log_group_tags
  log_stream = local.log_stream
  
  # Auto Scaling
  appautoscaling_target_min_capacity = var.fargate_appautoscaling_target_min_capacity
  appautoscaling_target_max_capacity = var.fargate_appautoscaling_target_max_capacity
  up_policy_name = local.up_policy_name
  up_policy_cooldown = var.fargate_up_policy_cooldown
  up_policy_metric_aggregation_type = var.fargate_up_policy_metric_aggregation_type
  down_policy_name = local.down_policy_name
  down_policy_cooldown = var.fargate_down_policy_cooldown
  down_policy_metric_aggregation_type = var.fargate_down_policy_metric_aggregation_type
  service_cpu_high_name = local.service_cpu_high_name
  service_cpu_high_comparison_operator = var.fargate_service_cpu_high_comparison_operator
  service_cpu_high_evaluation_periods = var.fargate_service_cpu_high_evaluation_periods
  service_cpu_high_period = var.fargate_service_cpu_high_period
  service_cpu_high_threshold = var.fargate_service_cpu_high_threshold
  service_cpu_high_statistic = var.fargate_service_cpu_high_statistic
  service_cpu_low_name = local.service_cpu_low_name
  service_cpu_low_comparison_operator = var.fargate_service_cpu_low_comparison_operator
  service_cpu_low_evaluation_periods = var.fargate_service_cpu_low_evaluation_periods
  service_cpu_low_period = var.fargate_service_cpu_low_period
  service_cpu_low_threshold = var.fargate_service_cpu_low_threshold
  service_cpu_low_statistic = var.fargate_service_cpu_low_statistic
}