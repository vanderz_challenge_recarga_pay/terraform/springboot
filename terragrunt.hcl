locals {
  resource_arn_association = "module.stage.stage_arn"
  rules = [
    {
      name = "AWS-AWSManagedRulesAdminProtectionRuleSet"
      priority = 1
      managed_rule_group_statement_name = "AWSManagedRulesAdminProtectionRuleSet"
      managed_rule_group_statement_vendor_name = "AWS"
      cloudwatch_metrics_enabled = false
      metric_name = "RecargaPay_WAF_AWSManagedRulesAdminProtectionRuleSet"
      sampled_requests_enabled = false
    },
    {
      name = "AWS-AWSManagedRulesAmazonIpReputationList"
      priority = 2
      managed_rule_group_statement_name = "AWSManagedRulesAmazonIpReputationList"
      managed_rule_group_statement_vendor_name = "AWS"
      cloudwatch_metrics_enabled = false
      metric_name = "RecargaPay_WAF_AWSManagedRulesAmazonIpReputationList"
      sampled_requests_enabled = false
    },
    {
      name = "AWS-AWSManagedRulesSQLiRuleSet"
      priority = 3
      managed_rule_group_statement_name = "AWSManagedRulesSQLiRuleSet"
      managed_rule_group_statement_vendor_name = "AWS"
      cloudwatch_metrics_enabled = false
      metric_name = "RecargaPay_WAF_AWSManagedRulesSQLiRuleSet"
      sampled_requests_enabled = false
    }
  ]
}

terraform {
  source = "git::https://gitlab.com/vanderz_challenge_recarga_pay/terraform/modules/aws/waf?ref=v1.0.0"
}

inputs = {
  waf_name = "recarga_pay"
  scope  = "REGIONAL"
  rules = local.rules
  cloudwatch_metrics_enabled  = false
  metric_name = "recarga_pay"
  sampled_requests_enabled  = false
  resource_arn_association = local.resource_arn_association
}