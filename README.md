# terraform/springboot

## CI/CD Pipeline

This project has terraform code for these AWS resources:

* ECS: deploys the API application into an ECS cluster
* NLB: load balances the ECS ingress traffic
* API Gateway: deploys API controls to the API application resources
* WAF: protects the API calls against some harmful web actions, like SQL Injection

A Gitlab backend is used to manage terraform state.

Five jobs are responsible to apply these terraform files to AWS:

* **init**: performs terraform init
* **validate**: performs terraform validate
* **build**: performs terraform plan
* **deploy**: performs terraform apply (manual execution)
* **cleanup**: performs terraform destroy (manual execution)

More details about the pipeline flow, see README from springboot repository:

https://gitlab.com/vanderz_challenge_recarga_pay/springboot

## ECS and WAF

The Pictures below show ECS metric dashboards that will be considered when scaling in or out the compute capacity:

![Alt text](images/ECS-Metricas-cluster.png?raw=true "ECS Metrics dashboard")
![Alt text](images/ECS-Metricas-graficos.png?raw=true "ECS Metrics dashboard")

Here some pictures about WAF protection to API Gateway:

![Alt text](images/WAF-Regras.png?raw=true "ECS Metrics dashboard")

![Alt text](images/WAF-APIGateway.png?raw=true "ECS Metrics dashboard")

## API performance calls

The picture below shows the latency obtained when calling the API resource GET /clientes severeal times (all of them less than 300ms):

![Alt text](images/postman-calls.png?raw=true "ECS Metrics dashboard")
