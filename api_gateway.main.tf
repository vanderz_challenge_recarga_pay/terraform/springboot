module "api" {
    source = "git::https://gitlab.com/vanderz_challenge_recarga_pay/terraform/modules/aws/api_gateway/api?ref=v1.0.0"

    api_name = var.api_gateway_rest_api_name
    endpoint_configuration = var.ap_gateway_rest_api_endpoint_configuration
}