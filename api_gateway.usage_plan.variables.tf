variable "api_gateway_usage_plan_name" {
  default = "RecargaPayPlan"
}

variable "api_gateway_usage_plan_description" {
  default = null
}

variable "api_gateway_usage_plan_product_code" {
  default = null
}
