locals {
  api_gateway_usage_plan_api_stages = {
    api_stages = {
      api_id = module.api.rest_api_id
      stage  = local.api_gateway_stage_name
    }
  }
  api_gateway_usage_plan_quota_settings = {}
  api_gateway_usage_plan_throttle_settings = {}
}
