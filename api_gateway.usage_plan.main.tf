module "usage_plan" {
    source = "git::https://gitlab.com/vanderz_challenge_recarga_pay/terraform/modules/aws/api_gateway/usage_plan?ref=v1.0.0"

    depends_on = [ module.stage ]

    usage_plan_name = var.api_gateway_usage_plan_name
    usage_plan_description = var.api_gateway_usage_plan_description
    usage_plan_product_code = var.api_gateway_usage_plan_product_code
    api_stages = local.api_gateway_usage_plan_api_stages
    quota_settings = local.api_gateway_usage_plan_quota_settings
    throttle_settings = local.api_gateway_usage_plan_throttle_settings
}

resource "aws_api_gateway_usage_plan_key" "main" {
  key_id        = module.api_key.api_key_id
  key_type      = "API_KEY"
  usage_plan_id = module.usage_plan.usage_plan_id
}