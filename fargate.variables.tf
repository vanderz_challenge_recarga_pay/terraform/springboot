variable "fargate_app_name" {}

variable "fargate_app_image" {}

variable "fargate_app_version" {}

variable "fargate_app_port" {
  default = 8080
}

variable "fargate_vpc_map" {}

variable "fargate_subnets_map" {}

variable "fargate_az_count" {
  default     = "2"
}

variable "fargate_app_count" {
  description = "Number of containers running"
  default = 1
}

variable "fargate_health_check_path" {
  default = "/health"
}

variable "fargate_health_check_grace_period_seconds" {
  default =  0
}

variable "fargate_healthy_threshold" {
  default = 3
}

variable "fargate_unhealthy_threshold" {
  default = 3
}

variable "fargate_health_interval" {
  default = 30
}

variable "fargate_health_matcher" {
  default = 200
}

variable "fargate_health_timeout" {
  default = 3
}

variable "fargate_cpu" {
  description = "Fargate instance CPU units to provision (1 vCPU = 1024 CPU units)"
  default = 512
}

variable "fargate_memory" {
  description = "Fargate instance memory to provision (in MiB)"
  default = 1024
}

variable "fargate_lb_type" {
  default = "network"
}

variable "fargate_lb_enable_deletion_protection" {
  default = false
}

variable "fargate_lb_internal" {
  default = true
}

variable "fargate_lb_port" {
  default = 8080
}

variable "fargate_lb_protocol" {
  default = "TCP"
}

variable "fargate_lb_listeners" {
  default = {
    8080 = {
      protocol = "TCP",
      ssl_policy = "",
      certificate_arn = ""
    }
  }
}

variable "fargate_log_retention_in_days" {
  default = 5
}

variable "fargate_appautoscaling_target_min_capacity" {
  description = "Minimal capacity of appautoscaling target"
  default = 1
}

variable "fargate_appautoscaling_target_max_capacity" {
  description = "Maximum capacity of appautoscaling target"
  default = 2
}

variable "fargate_up_policy_cooldown" {
  description = "Cooldown for up policy"
  default = 60
}

variable "fargate_up_policy_metric_aggregation_type" {
  description = "Metric aggregation type for up policy"
  default = "Maximum"
}

variable "fargate_down_policy_cooldown" {
  description = "Cooldown for down policy"
  default = 60
}

variable "fargate_down_policy_metric_aggregation_type" {
  description = "Metric aggregation type for down policy"
  default = "Maximum"
}

variable "fargate_service_cpu_high_comparison_operator" {
  description = "Comparison operator of high service cpu"
  default = "GreaterThanOrEqualToThreshold"
}

variable "fargate_service_cpu_high_evaluation_periods" {
  description = "evaluation periods of high service cpu"
  default = 2
}

variable "fargate_service_cpu_high_period" {
  description = "Periods of high service cpu"
  default = 60
}

variable "fargate_service_cpu_high_threshold" {
  description = "Threshold of high service cpu"
  default = 85
}

variable "fargate_service_cpu_high_statistic" {
  description = "Statistic of high service cpu"
  default = "Average"
}

variable "fargate_service_cpu_low_comparison_operator" {
  description = "Comparison operator of high service cpu"
  default = "LessThanOrEqualToThreshold"
}

variable "fargate_service_cpu_low_evaluation_periods" {
  description = "evaluation periods of high service cpu"
  default = 2
}

variable "fargate_service_cpu_low_period" {
  description = "Periods of high service cpu"
  default = 60
}

variable "fargate_service_cpu_low_threshold" {
  description = "Threshold of low service cpu"
  default = 10
}

variable "fargate_service_cpu_low_statistic" {
  description = "Statistic of low service cpu"
  default = "Average"
}

variable "fargate_security_group_balancer_description" {
  default = "LB Recarga Pay"
}

variable "fargate_ecs_sg_ingress_cidr_blocks" {
  type = list(string)
  default = ["0.0.0.0/0"]
}

variable "fargate_ecs_tasks_security_group_description" {
  default = "allow inbound access from the ALB only"
}
