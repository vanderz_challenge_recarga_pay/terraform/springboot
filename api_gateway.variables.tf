variable "api_gateway_rest_api_name" {
  default = "RecargaPay"
}

variable "ap_gateway_rest_api_endpoint_configuration" {
  default = {
    endpoint_configuration = {
      types = ["REGIONAL"]
    }
  }
}
