module "api_key" {
    source = "git::https://gitlab.com/vanderz_challenge_recarga_pay/terraform/modules/aws/api_gateway/api_key?ref=v1.0.0"

    api_key_name = var.api_gateway_api_key_name
    api_key_description = var.api_gateway_api_key_description
    api_key_enabled = var.api_gateway_api_key_enabled
}